function lowercase (input)
{
  if (!(typeof input === 'string'))
  {
    throw new Error('bad input');
  }
  return input.toLowerCase();
}
module.exports = lowercase;