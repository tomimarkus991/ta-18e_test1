const toNumber = require('./toNumber');

describe('toNumber', () => {
  test('1 => 1', () => {
    expect(toNumber(1)).toBe(1);
  });

  test('"1" => 1', () => {
    expect(toNumber('1')).toBe(1);
  });
  test('"a" is error', () => {
    expect(() => {
      toNumber('a');
    }).toThrow('value \'a\' is not a number!');
  });
  test('6 => 6', () => {
    expect(toNumber(6)).toBe(6);
  });
  test('"d" is error', () => {
    expect(() => {
      toNumber('d');
    }).toThrow('value \'d\' is not a number!');
  });
});
